import 'package:flutter/material.dart';
class Cards extends StatefulWidget {
  @override
  _CardsState createState() => _CardsState();
}

class _CardsState extends State<Cards> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Learning Different Layouts"),
      ),
      body: Container(
        padding: EdgeInsets.all(32.0),
        child: Center(
          child: SingleChildScrollView(
                      child: Column(
              children: [
                Text("Some Examples of Cards"),
                Card(
                  child: Container(
                    padding: EdgeInsets.all(32.0),
                    child: Column(
                      children: [
                        Text("Hello World"),
                        Text("Its Flutter For The World Now"),
                      ],
                    ),
                  ),
                ),
                Card(
                  child: Container(
                    padding: EdgeInsets.all(32.0),
                    child: Column(
                      children: [
                        Text("Hello World"),
                        Text("Let's Develop Apps Using Flutter"),
                      ],
                    
                    ),
                  ),
                ),
                            
              ],
            ),
          ),
        ),
      ),
    );
  }
}
