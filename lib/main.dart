import 'package:flutter/material.dart';
import 'card.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Learning About Layouts",
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController _user = TextEditingController();
  TextEditingController _pass = TextEditingController();
  String username = "";
  String password = "";
  void click() {
    setState(() {
      username = _user.text;
      password = _pass.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Learning Different Layouts"),
      ),
      body: Container(
        padding: EdgeInsets.all(32.0),
        child: Center(
          //used to give a scollable view to the column
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  "Please Login Here",
                  style: TextStyle(color: Colors.red, fontSize: 25.0),
                ),
                Row(
                  children: [
                    Text("Username"),
                    Padding(
                      padding: EdgeInsets.only(left: 15.0),
                    ),
                    Expanded(
                      child: TextField(controller: _user),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text("Password"),
                    Padding(
                      padding: EdgeInsets.only(left: 15.0),
                    ),
                    Expanded(
                      child: TextField(
                        controller: _pass,
                        obscureText: true,
                      ),
                    ),
                  ],
                ),

                Padding(
                  padding: EdgeInsets.all(12.0),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () => click(),
                      child: Text("Submit"),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _user.text = "";
                          _pass.text = "";
                        });
                      },
                      child: Text("Clear"),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(12.0),
                ),

                Text(
                  "Your Entered Username And Password",
                  style: TextStyle(color: Colors.red, fontSize: 20.0),
                ),
                Padding(
                  padding: EdgeInsets.all(12.0),
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        Text("Username: "),
                        Padding(
                          padding: EdgeInsets.only(left: 15.0),
                        ),
                        Text(username),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.all(12.0),
                    ),
                    Row(
                      children: [
                        Text("Password: "),
                        Padding(
                          padding: EdgeInsets.only(left: 15.0),
                        ),
                        Text(password),
                      ],
                    )
                  ],
                ),
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      username = "";
                      password = "";
                    });
                  },
                  child: Text("Clear"),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Cards()),
                    );
                  },
                  child: Text("Page 2"),
                ),
                //Card
              ],
            ),
          ),
        ),
      ),
    );
  }
}
